#!/bin/bash
IP_ADDRESS="localhost"

APACHE2_DIR="/etc/apache2"

UID_ROOT=0

if [ "$UID" -ne "$UID_ROOT" ]; then
  echo "$0 - Requires root privileges"
  exit 1
fi

function is_user()
{
    local check_user="$1";
    grep "$check_user:" /etc/passwd >/dev/null
    if [ $? -ne 0 ]; then
 #echo "NOT HAVE USER"
 return 0
    else
 #echo "HAVE USER"
 return 1
    fi
}
 
function generate_pass()
{
    CHARS="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()-_=+\\|/"
    LENGTH="8"
    while [ "${n:=1}" -le "$LENGTH" ] ; do
	PASSWORD="$PASSWORD${CHARS:$(($RANDOM%${#CHARS})):1}"
        let n+=1
    done
    echo $PASSWORD
}
 
function is_yes()
{
#TODO - add check 3-rd parameter for set default ansver (if press enter)
    while true
    do
 echo -n "Yes or No[Y/n]:"
 read  x
 if [ -z "$x" ]
 then
     return 0; #defaul answer: Yes
 fi
 case "$x" in
 y |Y |yes |Д |д |да ) return 0;;
 n |N |no |Н |н |нет ) return 1;;
# * ) ; # asc again
 esac
    done
}

function create_user()
{
    local login="$1"
    local password="$2"
    `useradd -m -s /bin/bash $login`
    #set password
    echo -e "$password\n$password\n" | passwd $login >> /dev/null
}


PUBLIC_DIR=$2



if [ $# -eq 2 ]; then

        SITE_NAME="$1.local"
        
        mkdir /var/www/$SITE_NAME
        mkdir /var/www/$SITE_NAME/www
        mkdir /var/www/$SITE_NAME/www/$PUBLIC_DIR
        mkdir /var/www/$SITE_NAME/logs
        mkdir /var/www/$SITE_NAME/tmp
        mkdir /var/www/$SITE_NAME/cgi-bin
        
        hostConf="
<VirtualHost ${IP_ADDRESS}:80>
        ServerName $SITE_NAME
        ServerAlias www.$SITE_NAME
        ServerAdmin webmaster@$SITE_NAME

        AddDefaultCharset utf-8
    
        DocumentRoot /var/www/$SITE_NAME/www/$PUBLIC_DIR
        CustomLog log combined
        ErrorLog /var/www/$SITE_NAME/logs/error.log
        DirectoryIndex index.php index.html

        ScriptAlias /cgi-bin/ /var/www/$SITE_NAME/cgi-bin
        <FilesMatch \"\\.ph(p[3-5]?|tml)$\">
                SetHandler application/x-httpd-php
        </FilesMatch>
        <FilesMatch \"\\.phps$\">
                SetHandler application/x-httpd-php-source
        </FilesMatch>
        CustomLog /var/www/$SITE_NAME/logs/access.log combined

        php_admin_value upload_tmp_dir "/var/www/$SITE_NAME/tmp"
        php_admin_value session.save_path "/var/www/$SITE_NAME/tmp"
        php_admin_value open_basedir "/var/www/$SITE_NAME/:."
</VirtualHost>
<Directory /var/www/$SITE_NAME/www/$PUBLIC_DIR>
        AllowOverride All
        Options +Includes +ExecCGI
        php_admin_flag engine on
</Directory>
        "

        touch ${APACHE2_DIR}/sites-available/${SITE_NAME}.conf
        echo "$hostConf" >> ${APACHE2_DIR}/sites-available/${SITE_NAME}.conf
        ln -s ${APACHE2_DIR}/sites-available/${SITE_NAME}.conf ${APACHE2_DIR}/sites-enabled/${SITE_NAME}.conf
        echo "127.0.0.1 $SITE_NAME" >> /etc/hosts

        touch //var/www/$SITE_NAME/www/$PUBLIC_DIR/index.php
        echo "<?php phpinfo() ?>" >> /var/www/$SITE_NAME/www/$PUBLIC_DIR/index.php

        chown -R www-data:www-data /var/www/$SITE_NAME 
        service apache2 restart
fi;

#display information
echo "*****************************************"
echo "* Profit!"
echo "*****************************************"
